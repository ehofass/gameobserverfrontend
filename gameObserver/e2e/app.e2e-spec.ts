import { GameObserverPage } from './app.po';

describe('game-observer App', function() {
  let page: GameObserverPage;

  beforeEach(() => {
    page = new GameObserverPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
