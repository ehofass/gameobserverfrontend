import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {FoosballService} from "./services/foosball.service";
import {FoosballComponent} from "./foosball-component/foosball-component.component";
import {TableTennisService} from "./services/tableTennis.service";
import {TableTennisComponent} from "./table-tennis-component/table-tennis-component.component";


@NgModule({
  declarations: [
    AppComponent,
    FoosballComponent,
    TableTennisComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  bootstrap: [AppComponent],
  providers: [FoosballService, TableTennisService]
})
export class AppModule { }
