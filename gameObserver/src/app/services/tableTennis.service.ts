import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Http} from "@angular/http";

@Injectable()
export class TableTennisService {
  tableTennisData:Array<any>;

  constructor(private http:Http) {
    this.tableTennisData =
    { gameType: "Table tennis",
      gameInProgress: false,
      secondsPlayed: '',
      matchNo: '',
      ETA: '',
      idleTime: "4:20"
    }
  }

  getTableTennisData() {
    return Observable
      .interval(2000)
      .flatMap(() => {
        return this.http.get("http://localhost:8080/game?id=2");
      }).subscribe(r => {
        console.log(r);
      });
  }

}
