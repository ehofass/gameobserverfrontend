import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Rx';


@Injectable()
export class FoosballService {
  foosballData:Array<any>;

  constructor(private http:Http) {
    this.foosballData =
    {
      gameType: "Foosball",
      gameInProgress: true,
      secondsPlayed: '1407',
      matchNo: '2',
      ETA: '4:20',
      idleTime: "00:00"
    }
  }

  getFoosballData() {
    return Observable
      .interval(2000)
      .flatMap(() => {
        return this.http.get("http://localhost:8080/game?id=1");
      }).subscribe(r => {
        console.log(r);
      });
  }
}
