import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {TableTennisService} from "../services/tableTennis.service";
import {TableTennisComponent} from "./table-tennis-component.component";

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ TableTennisComponent ],
  providers: [ TableTennisService]
})
export class TableTennisComponentModule {

}
