import { Component } from '@angular/core';
import { TableTennisService} from '../services/tableTennis.service';

@Component({
  selector: 'table-tennis-component',
  styles: [''],
  providers: [TableTennisService],
  templateUrl: '../foosball-component/foosball-component.html'
})

export class TableTennisComponent {
  componentName: 'TableTennisComponent';
  //variable assignation:
  constructor(_tennisService: TableTennisService) {
    this.serviceData = _tennisService.getTableTennisData();
  }
}
