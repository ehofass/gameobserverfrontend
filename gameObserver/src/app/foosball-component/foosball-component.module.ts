import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FoosballService} from "../services/foosball.service";
import {FoosballComponent} from "./foosball-component.component";

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ FoosballComponent ],
  providers: [ FoosballService]
})
export class FoosballComponentModule {

}
