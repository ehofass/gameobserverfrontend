import { Component } from '@angular/core';
import { FoosballService} from '../services/foosball.service';

@Component({
  selector: 'foosball-component',
  styles: [''],
  providers: [FoosballService],
  templateUrl: './foosball-component.html',
  styleUrls: ['./foosball-component.css']
})

export class FoosballComponent {
  componentName: 'FoosballComponent';
  //variable assignation:
  constructor(_FoosballService: FoosballService) {
    this.serviceData = _FoosballService.getFoosballData();
    this.testString = 'this is a param from the component';
  }
}
